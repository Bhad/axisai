from flask import Flask, render_template, request, redirect, jsonify, make_response
from werkzeug.utils import secure_filename
import uuid
from helpers import *
import numpy as np
from keras.models import load_model
from keras.layers import Input, Lambda
from keras.models import Model
import argparse

if not os.path.exists('temp'):
    os.makedirs('temp')

base_model = load_model('model/segnet_final_base_model.h5')
base_model.predict(np.zeros((1,155,220,1), dtype=int))

# input_a = Input(shape =(128,), name = 'ImageA_Input')
# input_b = Input(shape = (128,), name = 'ImageB_Input')

# distance = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape)([input_a, input_b])
# distance_model = Model(input=[input_a, input_b], output=distance)

# distance_model.predict([np.zeros((1,128), dtype=int), np.zeros((1,128), dtype=int)])

confidence_model = load_model('model/confidence-final-copy.hdf5')
confidence_model.predict([np.zeros((1,128), dtype=int), np.zeros((1,128), dtype=int)])

app = Flask(__name__)
app.config.from_object("config")

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/addUser", methods=["POST"])
def upload_file():

    if "user_file" not in request.files:
        return "No user_file key in request.files"

    file = request.files["user_file"]


    if "name" not in request.form:
        return "Please enter user name"

    user_name = request.form['name']
    unique_id = str(uuid.uuid4())

    if file and allowed_file(file.filename):
        addUserToDB(base_model, unique_id, user_name, file)
        upload_file_to_s3(file, app.config["S3_BUCKET"], unique_id)
    return "Success"


@app.route("/matchResults", methods=["POST"])
def match_results():

    if "user_file" not in request.files:
        return "No user_file key in request.files"

    file = request.files["user_file"]


    if "name" not in request.form:
        return "Please enter user name"

    user_name = request.form['name']
    unique_id = str(uuid.uuid4())

    if file and allowed_file(file.filename):
        upload_file_to_s3(file, app.config["S3_BUCKET"], unique_id)
        addUserToDB(unique_id, user_name)
    
    return "Success"

@app.route("/getUsers")
def getUsers():
    return jsonify(getUsersFromDB())

@app.route("/getUsersOriginalSign")
def getUsersOriginalSign():
    user = request.args.get('user')
    resp = make_response(getImage(user))
    resp.headers['Content-Type'] = 'application/jpg'
    return resp 

@app.route("/deleteUser")
def deleteUser():
    user = request.args.get('user')
    deleteUserFromDBAndS3(user)
    return "Success"

@app.route("/matchSignature", methods=["POST"])
def matchSignature():
    if "user_file" not in request.files:
        return "no user_file"

    custid = request.form.get('custid')
    file = request.files["user_file"]

    prediction = matchSignatures(confidence_model, base_model, custid, file)
    final_prediction = 0.0
    matched = False
    if prediction[0] > prediction[1]:
        if prediction[0] > 0.6:
            matched = True
            final_prediction = prediction[0]
        else:
            final_prediction = prediction[0]
    else:
        final_prediction = prediction[1]
    
    
    # if prediction[0] < prediction[0]:
    #     matched = True
    # prediction = ((((2 - float(prediction)) * (2 - float(prediction))) - 1) / 3) * 100

    return jsonify([matched, "{0:.2f}".format(round(float(final_prediction*100),2)) ])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run the server')
    parser.add_argument( "--host", default="localhost",help="host for deployment")
    parser.add_argument( "--port", default="5000",help="port for deployment")
    args = parser.parse_args()
    app.run(host=args.host, port=args.port)