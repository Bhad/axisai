defaultImage = "/static/assets/img/white.png"


var app = new Vue({
    el: '#vueApp',
    delimiters: ["[[","]]"],
    data: {
        user : {
            signature: defaultImage
        },
        originalSign : null,
        selectedUser: "",
        selectedId: "",
        matchPercent: 0.00,
        showScore: false,
        matched: false,
        alert: {
            hide : true,
            success : true,
            status: "",
            message : ""
        },
        users : []
    },
    methods:{
        submitNewUser: function(event){
            event.preventDefault();
            
            $('#addUser').modal('hide');
            $('#loading').modal('show');
            
            
            let formData = new FormData($('#new-user-form')[0]);
            
            $.ajax({
                url:  '/addUser', // point to server-side controller method
                dataType: 'text', // what to expect back from the server
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                type: 'post',
                success: function (response) {
                    console.log('SUCCESS!!');
                    $('#loading').modal('hide');
                    app.alert.hide = false
                    app.alert.success = true
                    app.alert.status = "SUCCESS"
                    app.alert.message = "User added successfully"
                    reloadUsers();
                },
                error: function (response) {
                    console.log('FAILURE!!');
                    $('#loading').modal('hide');
                    app.alert.hide = false
                    app.alert.success = false
                    app.alert.status = "ERROR"
                    app.alert.message = "Error while adding user"
                }
            });
            $("#new-user-form").trigger("reset");
            this.user.signature = defaultImage
            
        },
        
        addSignatureFile: function(event){
            this.user.signature = URL.createObjectURL(event.target.files[0]);
        },
        
        matchDialog: function(custid, username){
            this.showScore = false
            $("#signinput").val('')
            this.user.signature = defaultImage;
            this.originalSign = '/getUsersOriginalSign?user=' + custid;
            this.selectedUser = username
            this.selectedId = custid
            
        },
        
        deleteUser: function(custid, username){
            this.selectedId = custid
            this.selectedUser = username
        },
        
        confirmDelete : function(){
            $('#deleteUser').modal('hide');
            $('#loading').modal('show');
            $.get( '/deleteUser?user=' + this.selectedId, function( data ) {
                $('#loading').modal('hide');
                  reloadUsers();
            });
            
        },
        matchSignatures: function(event, custid){
            event.preventDefault();
            let formData = new FormData($('#match-form')[0]);
            
            formData.append("custid", custid);
            $('#loading').modal('show');
            
            $.ajax({
                url:  '/matchSignature', // point to server-side controller method
                dataType: 'text', // what to expect back from the server
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                type: 'post',
                success: function (response) {
                    console.log('SUCCESS!!');
                    app.showScore = true
                    var obj = JSON.parse(response);
                    app.matched = obj[0];
                    
                    app.matchPercent = obj[1];
                    setTimeout(function (){
                        $('#loading').modal('hide');

                    }, 300); 
                    
                },
                error: function (response) {
                    console.log('FAILURE!!');
                    $('#loading').modal('hide');
                    $('#matchSign').modal('hide');
                    app.alert.hide = false
                    app.alert.success = false
                    app.alert.status = "ERROR"
                    app.alert.message = "Error while matching user Signature"
                }
            });
        }
    }
})

function reloadUsers(){
    $('#loading').modal('show');
    $.getJSON( "/getUsers", function( data ) {
        app.users = data;
        setTimeout(function (){
            $('#loading').modal('hide');
        }, 300); 
    });
}
reloadUsers()
