import cv2
import numpy as np
from os import listdir
from os.path import isfile, join



orig_data = '/Users/naveekum/Downloads/DataBase'
test_data = '/Users/naveekum/Downloads/Test'
onlyfiles = [f for f in listdir(orig_data) if isfile(join(orig_data, f))]
onlyfiles_test = [f1 for f1 in listdir(test_data) if isfile(join(test_data, f1))]

def featurepoints(dictionary_names):
  
  dictionary_thresh={}
  for key in dictionary_names:
    descs=[]
    for filename in dictionary_names[key]:
      #print(filename)
      img = cv2.imread(orig_data+'/'+filename)
      
      sift = cv2.xfeatures2d.SIFT_create(edgeThreshold = 15)

      kp, des = sift.detectAndCompute(img,None)
      newdes = np.mean(des, axis=0)
      descs.append(newdes)
      

       
    dist1 = np.linalg.norm(descs[0]-descs[1])
    dist2 = np.linalg.norm(descs[0]-descs[2])
    dist3 = np.linalg.norm(descs[1]-descs[2])
      
    print(dist1, dist2,dist3)
    dictionary_thresh[key]=max(dist1,dist2, dist3)
  return dictionary_thresh


def getsift(impath):
  img = cv2.imread(impath)
  sift = cv2.xfeatures2d.SIFT_create(edgeThreshold = 15)

  kp, des = sift.detectAndCompute(img,None)
  newdes = np.mean(des, axis=0)
  return newdes

def getSiftdistance(im1,im2):
  d1 = getsift(orig_data+'/'+im1)
  d2= getsift(test_data+'/'+im2)

  return np.linalg.norm(d1-d2)




key_names=[]
for file in onlyfiles:
  key_names.append(file.split('_')[0])

uniquekeys = set(key_names)
dictionary_names={}
for key in uniquekeys:
  dictionary_names[key]=[]
  for file in onlyfiles:
    if key in file:
      dictionary_names[key].append(file)



desc_orig = featurepoints(dictionary_names)

print(desc_orig)

file_dictionary_csv={}
counter=0
test = open('op_final.csv', 'w')
for key in desc_orig:
  for file in onlyfiles_test:
    #print(key,file)
    if key in file:
      counter+=1
      print(key, file)
      key_distances=[]
      for r1 in dictionary_names[key]:

        distance = getSiftdistance(r1, file)
        key_distances.append(distance)

      file_distance = max(key_distances[0], key_distances[1],key_distances[2])
      print(file_distance, desc_orig[key])
      if file_distance > desc_orig[key]:
        file_dictionary_csv[file]='Yes'
      else:
        file_dictionary_csv[file]='No'

      test.write(file+','+file_dictionary_csv[file]+'\n')
      print(file_dictionary_csv[file])
      print(counter)
test.close()

'''
test = open('op.csv', 'w')
for key in file_dictionary_csv:
  test.write(key+','+file_dictionary_csv+'\n')
test.close()'''







'''
desc_forge = featurepoints(mypath_dupe)

print(np.array(desc_orig).shape, np.array(desc_forge).shape)

distances=[]
for orig in desc_orig:
  for forge in desc_forge:
    dist = np.linalg.norm(orig-forge)
    distances.append(dist)
print(np.linalg.norm(desc_orig[0]-desc_orig[1]))
print(distances)

for key in dict_keys:
  print(dict_keys[key])'''







