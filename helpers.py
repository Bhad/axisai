import boto3, botocore
from config import S3_BUCKET, DB_TABLE
from keras.preprocessing import image
import os
import numpy as np
from decimal import Decimal
from keras import backend as K

std = np.array([[[38.684277]]])
height = 155
width = 220

s3 = boto3.client("s3")
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(DB_TABLE)
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


def upload_file_to_s3(file, bucket_name, cust_id):
    
    try:

        s3.upload_file(
            'temp/{}uploaded.jpg'.format(cust_id),
            bucket_name,
            'custsignimg/' + cust_id
        )
    
    except Exception as e:
        # This is a catch all exception, edit this part to fit your needs.
        print("Error while uploading file to s3 : ", e)
        return e
    os.remove('temp/{}uploaded.jpg'.format(cust_id))

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def addUserToDB(base_model, cust_id, user_name, file):
    
    file.save('temp/{}uploaded.jpg'.format(cust_id))
    img = image.img_to_array(image.load_img('temp/{}uploaded.jpg'.format(cust_id), color_mode = "grayscale", target_size=(height, width))) / (std + K.epsilon())
    prediction = base_model.predict([img.reshape(1,155,220,1)])
    pred_list = prediction[0].tolist()
    pred_list = [Decimal(pred) for pred in pred_list]
    table.put_item(
    Item={
            'custid': cust_id,
            'username' : user_name,
            'signature' : pred_list
        }
    )

def getUsersFromDB():
    response = table.scan()
    items = response['Items']
    items = [{'username': item['username'], 'custid': item['custid']} for item in items]
    return items

def getImage(custid):
    obj = s3.get_object(Bucket=S3_BUCKET, Key='custsignimg/' + custid)
    return obj['Body'].read()

def deleteUserFromDBAndS3(custid):
    table.delete_item(Key={
        'custid': custid,
    })
    s3.delete_object(Bucket=S3_BUCKET,Key='custsignimg/' + custid)

def matchSignatures(confidence_model, base_model, custid, file):

    item = table.get_item(Key={
        'custid': custid,
    })

    original_signature_features = item['Item']['signature']
    original_signature_features = np.asarray([[float(feature) for feature in original_signature_features]])
    file.save('temp/{}uploaded.jpg'.format(custid))
    img = image.img_to_array(image.load_img('temp/{}uploaded.jpg'.format(custid), color_mode = "grayscale", target_size=(height, width))) / (std + K.epsilon())

    base_prediction = base_model.predict([img.reshape(1,155,220,1)])
    prediction = confidence_model.predict([original_signature_features, base_prediction])
    os.remove('temp/{}uploaded.jpg'.format(custid))
    print(prediction[0])
    return prediction[0]


def euclidean_distance(vects):
    x, y = vects
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True))
    
def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)