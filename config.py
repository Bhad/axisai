import os

S3_BUCKET                 = "axisai"
DB_TABLE                  = "axisai"
SECRET_KEY                = os.urandom(32)
DEBUG                     = True
PORT                      = 5000